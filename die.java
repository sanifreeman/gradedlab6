import java.util.Random;
public class die{
    private int faceValue;
    private Random random;

    public die(){
        this.faceValue=1;
        this.random= new Random();
    }

    public int getFaceValue(){
        return this.faceValue;
    }

    public void roll(){
        this.faceValue= random.nextInt(6)+1;
    }
    public String toString(){
        return Integer.toString(faceValue);
    }
}