public class jackpot{
    public static void main(String[]args){
        System.out.println("Welcome to the game!");
        Board board = new Board();
        boolean gameOver= false;
        int numOfTilesClosed=0;

        while(!gameOver){
            System.out.println(board);
            if(board.playAturn()){
                gameOver=true;
            }else{
                numOfTilesClosed++;
                }
        }
        if(numOfTilesClosed>=7){
            System.out.println("you won, you reached the Jackpot !!");
        }else{
            System.out.println("Loser, do better.");
        }
    }
}
